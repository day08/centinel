
const Socket = require('socket.io');;

const Express = require('express');
const fs = require('fs');
// const { Socket } = require('dgram');

const app = Express();
// const server = http.Server(app);

const Server = require('http').Server(app)
const io = Socket(Server, {cors: {origins: '*'}})

const sp = require('./webSocket.js');
// const initBot = require('./bots/bot.telegram');

const NOTIFICATION_PATH = './notifications.json';
const NOTIFICATION_ACTIVE_PATH = './notifications_active.json';
const SESSIONS_PATH = './sessions_active.json';
const TYPE_EVENTS = {
            'claimer': 'claimer',
            'deposit': 'deposit',
            'service_payment': 'service_payment',
            'ticket_support_response': 'ticket_support_response',
        
        };
let connected = {};
let userList = new Map();

// initBot();

io.use((socket, next) => {
    socket.conn.transport.once('headers', (headers) => {
        headers['set-cookie'] ="sess=test;"; });

    next();
});

require('dotenv').config();
app.set('port', process.env.PORT || 3000);
app.use(Express.static(__dirname + '/public'));

Server.listen(app.get('port'),() => {
    
    console.log("servidor corriendo en puerto:" + app.get('port'));
   
})

io.on('connection', (socket) => {
    let notifications = notify();
    
    io.emit('notify', notifications);

    // setTimeout(()=>{
    //     console.log('borrando lista de usuarios')
    //     userList.clear()
    // }, 60000);
    
    socket.on('welcome', (msj) => {
        console.log(msj);
        io.emit('welcome', 'Hola desde el servidor')
    });

    socket.on('is_connected', (user)=> {
        return false;

    });

    socket.on('join', (data, callback)=>{
        
        let token = get_session({name: data.user, item:'token'});
        let room = `${data.room}/${data.user.replace(' ', '').trim()}_${token}`;
        if (!userList.has(data.user)) {
            
            socket.join(room);
            userList.set(data.user, [{room: room, origin:'root'}]);
        }else {

            socket.join(room);
            userList.get(data.user).push({room: room, origin:'copy'}); 
            let origin_root = userList.get(data.user).filter(room=> room.origin === 'root')[0]?.room;
            
            io.to(origin_root).emit('session_active', {user: data.user, message: 'Alguien a accedido con tus datos a la plataforma.'});
        }

        socket.off('join');
    });

    socket.on('leave_room', (data) => {

        let token = get_session({name: data.user, item:'token'});
        let room_user = `${data.room}/${data.user.replace(' ', '').trim()}_${token}`;
        if (userList.has(data.user)) {
            
            let room = userList.get(data.user).filter(room=> room.room === room_user)[0]?.room;
            console.log(`${data.user} ha abandonado la habitacion: ${room}`)
            socket.leave(room);
        }else {
            return false;
        }
    });

    socket.on('notification', (data)=>{
        // 
            let notifications;
            let new_notification = {
                where: data.where,
                count: data.count,
                persons: data.persons,
                personsViews: data.personsViews,
                personsSustractCount: data.personsSustractCount
            }
            if (fs.existsSync(NOTIFICATION_PATH) && Object.keys(fs.readFileSync(NOTIFICATION_PATH)).length) {
               
                try{
                     notifications = JSON.parse(fs.readFileSync(NOTIFICATION_PATH));
                }catch (e) {
                    return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                }
                
                let found=[];
               notifications.forEach(notification=>{
                    found.push(notification.where);
                    if (notification.where === data.where && notification.personsViews < 6) {
                            notification.count +=1;

                    }else if (notification.where === data.where && notification.personsViews >= 6) {
                            notification.count = 0;
                            notification.persons = [];
                            notification.personsViews = 0;
                            notification.personsSustractCount = {};
                    }

                    
                })

                if (!found.includes(data.where)){
                    notifications = [...notifications, new_notification];
                }

                fs.writeFileSync(NOTIFICATION_PATH, JSON.stringify(notifications), function(error) {
                    if (error) {
                        return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                    }
                })
                
            }else {
                
                fs.writeFileSync(NOTIFICATION_PATH, JSON.stringify([new_notification]), function(error) {
                    if (error) {
                        return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                    }
                })
            }

            setTimeout(()=> {
                io.emit('notify', notifications);
            }, 3000);
    });

    socket.on('update_notification', (data=> {
        let notifications;
        if (fs.existsSync(NOTIFICATION_PATH) && Object.keys(fs.readFileSync(NOTIFICATION_PATH)).length) {
            try{
                 notifications = JSON.parse(fs.readFileSync(NOTIFICATION_PATH));
            }catch (e) {
                return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
            }
            
           notifications.forEach(notification=>{
                if (notification.where === data.where && notification.personsViews < 6 && notification.persons.includes(data.persons)) {
                        notification.personsSustractCount[data.persons] = notification.count;

                }else if (notification.where === data.where && notification.personsViews >= 6) {
                    notification.count = 0;
                    notification.persons = [];
                    notification.personsViews = 0;
                    notification.personsSustractCount = {};

                }else if (notification.where === data.where && notification.personsViews < 6 && !notification.persons.includes(data.persons)) {
                        notification.persons.push(data.persons);
                        notification.personsViews = notification.persons.length;
                        notification.personsSustractCount[data.persons] = notification.count;
                    }

                fs.writeFileSync(NOTIFICATION_PATH, JSON.stringify(notifications), function(error) {
                    if (error) {
                        return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                    }
                })
            })
        }else {
            return {error: 'FILE_NOT_FOUND', message: 'El archivo no existe'}
        }
    }));

    socket.on('notification_active', (data)=>{

            let notifications_active;
            let new_notification = {
                person: data.person,
                notifications: data.notifications,
            }
        if (fs.existsSync(NOTIFICATION_ACTIVE_PATH) && Object.keys(fs.readFileSync(NOTIFICATION_ACTIVE_PATH)).length) {
               
            try{
                 notifications_active = JSON.parse(fs.readFileSync(NOTIFICATION_ACTIVE_PATH));
            }catch (e) {
                return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
            }
            
            let found=[];
            notifications_active.forEach(notification=>{
                found.push(notification.person);
                if (notification.person === data.person) {
                    notification.notifications = data.notifications
                }
            })

            if (!found.includes(data.person)){
                notifications_active = [...notifications_active, new_notification];
            }

            fs.writeFileSync(NOTIFICATION_ACTIVE_PATH, JSON.stringify(notifications_active), function(error) {
                if (error) {
                    return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                }
            })
            
        }else {
            
            fs.writeFileSync(NOTIFICATION_ACTIVE_PATH, JSON.stringify([new_notification]), function(error) {
                if (error) {
                    return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                }
            })
        }
    });

    socket.on('tellMeSomething', (data)=>{
        try{
            notifications_active = JSON.parse(fs.readFileSync(NOTIFICATION_ACTIVE_PATH));
       }catch (e) {
           return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
       }
        socket.broadcast.emit('new action', {
            data,
            notifications_active
        });
    });

    socket.on('get_notifications_status', (data)=>{
        let persons = [];
        let notifications_active;
        let new_notification = {
            person: data.person,
            notifications: true,
        }

        socket.join(data.room);
        if (fs.existsSync(NOTIFICATION_ACTIVE_PATH) && Object.keys(fs.readFileSync(NOTIFICATION_ACTIVE_PATH)).length) {
            try{
                    notifications_active = JSON.parse(fs.readFileSync(NOTIFICATION_ACTIVE_PATH));
            
                notifications_active.forEach(notification=> {
                    persons.push(notification.person);
                });

                if (!persons.includes(data.person)) {
                    notifications_active = [...notifications_active, new_notification];

                    fs.writeFileSync(NOTIFICATION_ACTIVE_PATH, JSON.stringify(notifications_active), function(error) {
                        if (error) {
                            return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
                        }
                    })
                }
            console.log(notifications_active);
            notifications_active = notifications_active.filter(p=>p.person===data.person)[0];
            }catch (e) {
                return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
            }
    }else{
        fs.writeFileSync(NOTIFICATION_ACTIVE_PATH, JSON.stringify([new_notification]), function(error) {
            if (error) {
                return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
            }
        });
        try{
            notifications_active = JSON.parse(fs.readFileSync(NOTIFICATION_ACTIVE_PATH));

            console.log(notifications_active);
            notifications_active = notifications_active.filter(p=>p.person===data.person)[0];
        }catch(e){

            return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
        }
    }

       
         io.to(data.room).emit('get_notifications_status', notifications_active);
        socket.leave(data.room);
    });

    socket.on('transaction_product', (data) => {
        
        socket.broadcast.emit('transaction_product', {
            message: 'nueva transaccion de producto realizada', 
            data: data
        });
    });

    socket.on('typing', (data) => {
        
        socket.broadcast.emit('typing', {
            message: 'someOne set state typing', 
            data: data
        });
    });

    socket.on('biyuyo_online_events', (event) => {
        
        const {event_name, data} = event;

       try{
            let e = TYPE_EVENTS[event_name];
            socket.broadcast.emit(e, data);
       }catch {
        socket.emit('biyuyo_online_events', {
            error: true,
            message: 'EVENT_NOT_EXIST'
        })
       }

    });

<<<<<<< HEAD
    socket.on('new_post', (message)=> {
        socket.broadcast.emit('new_post', message);
=======
    socket.on('viewIncrement', (name)=> {
        io.emit('viewIncrement', name);
    });

    socket.on('new_post', (message)=> {
        io.emit('new_post', message);
>>>>>>> master
    });
});



function addUser(username, room) {
    if (!userList.has(username)) {
        
        userList.set(username, room);
    }else {
        let v = Object.keys(userList).length;
        let userCopy = `${username}${v}`;
        let roomCopy = `${room}${userCopy}`;
        userList.set(userCopy,roomCopy);
    }
}

function storage(data) {
    console.log('storage');
        let token = get_session({name: data.user, item:'token'});
        let room = `${data.room}/${data.user.replace(' ', '').trim()}_${token}`;
        if (!userList.has(data.user)) {
            
            socket.join(room);
            userList.set(data.user, [{room: room, origin:'root'}]);
        }else {
            socket.join(room);
            userList.get(data.user).push({room: room, origin:'copy'}); 
            let origin_root = userList.get(data.user).filter(room=> room.origin === 'root')[0]?.room;
            
            io.to(origin_root).emit('session_active', {user: data.user, message: 'Alguien a accedido con tus datos a la plataforma.'});
        }

        console.log(userList);
        console.log(`${data.user} se union a la habitacion ${room}`)
}

function get_session(data) {
    const {name, item} = data;
    try{
        sessions =  JSON.parse(fs.readFileSync(SESSIONS_PATH));
        
        sessions = sessions.filter(p=>p.name===name)[0];
        }catch (e) {
            return {error: 'FILE_ERROR', message: 'El archivo no se puede leer'};
        }
        
    if (sessions) return sessions[item];
    return {'error': 'USER_UNAUTHORIZED', 'message': 'USUARIO NO REGISTRADO'}
}

function removeUser(username, id) {
    if (userList.has(username)) {
        let userIds = userList.get(username);
        if (userIds.size === 0) {
            userList.delete(username);
        }
    }
}

function notify() {
    try {
        if (fs.existsSync(NOTIFICATION_PATH)) {
            let notifications = JSON.parse(fs.readFileSync(NOTIFICATION_PATH));
            return notifications;
            
        }else {
            return {error: 'FILE_NOT_FOUND', message: 'El archivo no existe'};
        }
    }catch (e) {
        
        return {error: 'FILE_EMPTY', message: 'Sin notificaciones'};
    }
}

