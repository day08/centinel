FROM node:latest


LABEL authors="4Freek" 

RUN npm -g install npm@latest
COPY package.json .
RUN npm install
WORKDIR /
COPY . .


# FROM nginx:alpine
# COPY . /usr/share/nginx/html
ENTRYPOINT [ "node" ]
